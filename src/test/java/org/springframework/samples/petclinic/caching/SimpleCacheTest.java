package org.springframework.samples.petclinic.caching;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.samples.petclinic.model.Owner;
import org.springframework.samples.petclinic.model.Person;
import org.springframework.samples.petclinic.util.ApplicationSwaggerConfig;

import javax.persistence.EntityManager;

import java.util.Map;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.atLeast;


//@PrepareForTest(Plane.class)
//@Profile("UserServiceImpl-test")

//@SpringBootTest(classes = ApplicationSwaggerConfig.class)
@RunWith(MockitoJUnitRunner.class)
public class SimpleCacheTest {
    @InjectMocks
    SimpleCache simpleCache;

    @Mock
    EntityManager entityManager;

    @Mock
    Map<EntityIdentity, Object> cache;
    @Mock
    Object id= 4;
    @Spy
    EntityIdentity entityIdentity = new EntityIdentity(Person.class, id);

//    @Mock
//    Class<Integer> clazz;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(cache.containsKey(entityIdentity)).thenReturn(false);
        when(cache.get(entityIdentity)).thenReturn("testString");
        when(cache.remove(entityIdentity)).thenReturn("susses");

    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void retrieve() {

        simpleCache.retrieve(Person.class, id);
//        verify(cache, atLeastOnce()).containsKey(entityIdentity);
        verify(entityManager, atLeastOnce()).find(Person.class, id);
//        verify(cache, atLeastOnce()).put(entityIdentity, id);

    }

    @Test
    public void evict() {

        simpleCache.evict(Person.class, id);
//        final Object remove = verify(cache).remove(entityIdentity);
        verify(cache).remove(new EntityIdentity(Person.class, id));
    }
}
